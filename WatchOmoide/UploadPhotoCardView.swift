//
//  UploadPhotoCardView.swift
//  Omoide
//
//  Created by Octavianus Bastian on 08/06/21.
//

import SwiftUI

struct UploadPhotoCardView: View {
    let category: Category
    var body: some View {
        if (category.image.isEmpty) {
            VStack{
                HStack {
                    Button(action: /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/{}/*@END_MENU_TOKEN@*/) {
                        Text("+").font(.title)
                    }
                    .foregroundColor(Color("AccentColor")).padding(.horizontal, 8)
                    .padding(.top, -3)
                }.background(Color.white).cornerRadius(50).padding()
                
            }.padding(.vertical, 38).background(LinearGradient(gradient:
                                                                Gradient(colors: [Color(ColorsSaved.primaryColor), Color(ColorsSaved.secondaryColor)]),
                                                               startPoint: .top,
                                                               endPoint: .bottom).cornerRadius(14).frame(width: 95, height: 123).shadow(radius: 4, x: 0, y: 2) .overlay(
                                                                RoundedRectangle(cornerRadius: 16)
                                                                    .stroke(Color.white, lineWidth: 1)
                                                               ))
        }
        else {
            VStack{
                HStack{
                    Image(category.image)
                        .resizable().scaledToFill()
                        .frame(width: 95, height: 123)
                        .cornerRadius(14).shadow(radius: 4, x: 0, y: 2)}
                ZStack {
                    Button(action: /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/{}/*@END_MENU_TOKEN@*/) {
                        Text("-")
                            .font(.title)
                    }
                    .padding(.horizontal, 8)
                    .padding(.top, -3)
                }.background(Color.white).cornerRadius(50).padding(.top, -80)
            }
            
        }
        
    }
}

struct UploadPhotoCardView_Previews: PreviewProvider {
    static var previews: some View {
        UploadPhotoCardView(category: mCategoryData[0])
    }
}
