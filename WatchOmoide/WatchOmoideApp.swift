//
//  WatchOmoideApp.swift
//  WatchOmoide
//
//  Created by Octavianus Bastian on 14/06/21.
//

import SwiftUI

@main
struct WatchOmoideApp: App {
    
    let persistenceController = PersistenceController.shared
    
    var body: some Scene {
        WindowGroup {
            Signin().environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}


