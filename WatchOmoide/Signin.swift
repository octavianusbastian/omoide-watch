//
//  Signin.swift
//  Omoide
//
//  Created by Natanael Kenan Gunawan on 08/06/21.
//

import SwiftUI
import AuthenticationServices

struct Signin: View {
    var body: some View {
        NavigationView{
            ZStack{
                Image("bgmantap")
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: UIScreen.main.bounds.width)
                    .overlay(Color.black.opacity(0.65))
                    .ignoresSafeArea()
                
                VStack(spacing: 25){
                    Text("Omoide")
                        .font(.system(size: 40))
                        .fontWeight(.heavy)
                        .foregroundColor(.white)
                        .frame(maxWidth: .infinity, alignment: .center)
                        .padding()
                    
                    Spacer()
                    
                    VStack(alignment: .leading, spacing: 30
                           
                           , content: {
                            
                            Text("Happier Day With Omoide")
                                .font(.system(size: 30))
                                .fontWeight(.bold)
                                .foregroundColor(.white)
                            
                            Text("Memories Bring Happiness")
                                .fontWeight(.semibold)
                                .foregroundColor(.white).padding(.top, -20)
                           })
                    
                    Spacer()
                    
                    
                    //                    SignInWithAppleButton { (request) in
                    //
                    //                        request.requestedScopes = [.email,.fullName]
                    //
                    //                    } onCompletion: { (result) in
                    //                    }
                    //                    .signInWithAppleButtonStyle(.white)
                    //                    .frame(height: 55)
                    //                    .clipShape(Capsule())
                    //                    .padding(.horizontal,40)
                    //                    .offset(y:-70)
                    
                    NavigationLink(destination: BridgeView()) {
                        Text("Sign in with Apple")
                            .font(.title2)
                            .fontWeight(.semibold)
                            .padding(.vertical, 3).foregroundColor(Color.black).frame(width: 320, height:30).navigationBarTitle("")
                            .navigationBarBackButtonHidden(true)
                            .navigationBarHidden(true)
                        
                    }.padding(.vertical, 10).background(Color.white).cornerRadius(30)
                }
                
            }
        }
        
    }
}

struct Signin_Previews: PreviewProvider {
    static var previews: some View {
        Signin()
    }
}
