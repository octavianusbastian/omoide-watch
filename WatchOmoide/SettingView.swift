//
//  SettingView.swift
//  Omoide
//
//  Created by Natanael Kenan Gunawan on 11/06/21.
//

import SwiftUI


struct SettingView: View {
    
    @State private var editprofile = ""
    @State private var notification = ""
    @State private var language = ""
    @State private var previewIndex = 0
    @Binding var showSettingView: Bool 
    
    var previewOptions = ["Always", "When Unlocked", "Never"]
    var previewbhs = ["English", "Japanese", "Bahasa Indonesia"]
    var haloo = ["Knock Knock", "Konnichiwa", "Ding Dong"]
    
    var body: some View {
        
        NavigationView{
            
            Form{
                
                Section(header: Text("Edit")){
                    Picker(selection: $previewIndex, label: Text("Notification")) {
                        ForEach(0 ..< previewOptions.count) {
                            Text(self.previewOptions[$0])
                            
                            
                        }
                    }
                    Picker(selection: $previewIndex, label: Text("Language")) {
                        ForEach(0 ..< previewbhs.count) {
                            Text(self.previewbhs[$0])
                            
                            
                        }
                    }
                    Picker(selection: $previewIndex, label: Text("Sentences")) {
                        ForEach(0 ..< haloo.count) {
                            Text(self.haloo[$0])
                            
                            
                        }
                    }
                }
                Section(header: Text("ABOUT")) {
                    HStack {
                        Text("Version")
                        Spacer()
                        Text("0.0.1")
                    }
                }
            }
            .navigationBarTitle(Text("Setting"), displayMode: .inline).navigationBarItems(trailing: Button(action: {
                print("Dismissing sheet view...")
                self.showSettingView = false
            }) {
                Text("Done").bold()
            })
        }
    }
}

//struct SettingView_Previews: PreviewProvider {
//    static var previews: some View {
//        SettingView()
//    }
//}
