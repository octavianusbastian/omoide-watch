//
//  ContentView.swift
//  Omoide
//
//  Created by Octavianus Bastian on 06/06/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        
        
        Memories().navigationBarBackButtonHidden(true)
            .navigationBarHidden(true)
        
        
    }
    
    
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
