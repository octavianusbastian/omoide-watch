//
//  BridgeView.swift
//  Omoide
//
//  Created by Octavianus Bastian on 08/06/21.
//

import SwiftUI

struct BridgeView: View {
    @State var text = ""
    let items = Array(1...12).map({ "image\($0)" })
    
    let layout = [
        GridItem(.flexible(minimum: 100)),
        GridItem(.flexible(minimum: 100)),
        GridItem(.flexible(minimum: 100))
    ]
    var columns = Array(repeating: GridItem(.flexible()), count: 3)
    var body: some View {
        NavigationView {
            VStack {
                VStack(alignment: .leading) {
                    HStack {
                        Text("Hi")
                            .font(.title2)
                            .fontWeight(.regular)
                        Text("Steven,")
                            .font(.title2)
                            .fontWeight(.bold)
                            .multilineTextAlignment(.leading)
                    }
                    HStack {
                        Text("Choose at least 2 photos of memorable moments that you have")
                            .font(.subheadline)
                            .lineSpacing(5)
                    }.padding(.vertical, 1)
                }.padding(.horizontal, 10).padding(.top, 20)
                VStack{
                    ScrollView(.vertical, showsIndicators: false){
                        LazyVGrid(columns: columns){
                            ForEach(mCategoryData.filter({"\($0)".lowercased().contains(text.lowercased()) || text.isEmpty})){ i in
                                UploadPhotoCardView(category: i)
                            }
                        }
                    }
                }.frame(
                    minWidth: 0,
                    maxWidth: .infinity,
                    minHeight: 0,
                    maxHeight: .infinity,
                    alignment: .topLeading
                ).padding(.horizontal, 10)
                .padding(.vertical, 10)
                VStack {
                    HStack {
                        Text("1 / 2").font(.footnote).fontWeight(.regular).foregroundColor(Color.gray)
                    }.padding(.vertical, 10)
                    
                    HStack {
                        NavigationLink(destination: BridgeViewSpouse()) {
                            Text("Next").padding(.vertical, 3).foregroundColor(Color.white).frame(minWidth: 0, maxWidth: .infinity).navigationBarTitle("")
                                .navigationBarBackButtonHidden(true)
                                .navigationBarHidden(true)
                            
                        }.padding(.vertical, 10).background(Color("AccentColor")).cornerRadius(14)
                        //                    Button(action: {}) {
                        //                        Text("Next").padding(.vertical, 3).foregroundColor(Color.white).frame(minWidth: 0, maxWidth: .infinity)
                        //                    }
                        //                    .padding(.vertical, 10).background(Color("AccentColor")).cornerRadius(14)
                    }.padding(.horizontal, 10).padding(.bottom, 10)}
            }
        }.navigationBarBackButtonHidden(true)
        .navigationBarHidden(true)
    }
}

struct BridgeView_Previews: PreviewProvider {
    static var previews: some View {
        BridgeView()
    }
}
