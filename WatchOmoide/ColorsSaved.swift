//
//  ColorsSaved.swift
//  Omoide
//
//  Created by Octavianus Bastian on 09/06/21.
//

import Foundation
import UIKit

struct ColorsSaved {
static let primaryColor = UIColor( red: 50/255, green: 215/255, blue: 75/255, alpha: 1.0)
static let secondaryColor = UIColor( red: 98/255, green: 221/255, blue: 199/255, alpha: 1.0)
}
