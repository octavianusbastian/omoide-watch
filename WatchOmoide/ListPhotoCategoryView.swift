//
//  ListPhotoCategoryView.swift
//  Omoide
//
//  Created by Steven Tan on 11/06/21.
//
import SwiftUI
struct ListPhotoCategoryView: View {
    
    @State var text = ""
    let Poto = Array(1...50).map({ "Image\($0)" })
    
    var columns = Array(repeating: GridItem(.flexible()), count: 3)
    
    var body: some View {
        NavigationView {
            ScrollView(.vertical, showsIndicators: false){
                VStack{
                    LazyVGrid(columns: columns) {
                        ForEach(DataPhoto.filter({"\($0)".lowercased().contains(text.lowercased()) || text.isEmpty})){ i in
                            PhotoCell(ListPhoto: i)
                        }.padding(EdgeInsets(top: 0, leading: 0, bottom: -6, trailing: 0))
                    }
                }
            }.navigationTitle("Special Person")
        }
    }
}

struct ListPhotoCategoryView_Previews: PreviewProvider {
    static var previews: some View {
        ListPhotoCategoryView()
    }
}

struct ListPhotoCategory: Identifiable{
    var id = UUID()
    var Category: String
    var Image: String
}

var DataPhoto = [
    ListPhotoCategory(Category: "Special Person", Image: "Photo1"),
    ListPhotoCategory(Category: "Special Person", Image: "Photo2"),
    ListPhotoCategory(Category: "Special Person", Image: "Photo3"),
    ListPhotoCategory(Category: "Special Person", Image: "Photo4"),
    ListPhotoCategory(Category: "Special Person", Image: "Photo5"),
    ListPhotoCategory(Category: "Special Person", Image: "Photo6"),
    ListPhotoCategory(Category: "Special Person", Image: "Photo7"),
    ListPhotoCategory(Category: "Special Person", Image: "Photo8"),
    ListPhotoCategory(Category: "Special Person", Image: "Photo9"),
    ListPhotoCategory(Category: "Special Person", Image: "Photo10"),
]
