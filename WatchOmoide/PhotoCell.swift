//
//  PhotoCell.swift
//  Omoide
//
//  Created by Steven Tan on 11/06/21.
//

import SwiftUI

struct PhotoCell: View {
    
    let ListPhoto: ListPhotoCategory
    var body: some View {
        VStack(alignment: .leading, content: {
            ZStack{
                Image(ListPhoto.Image)                    .resizable()
                    .scaledToFill()
                    .frame(width: 131, height: 131, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    .cornerRadius(2)
            }
        })
    }
}

struct PhotoCell_Previews: PreviewProvider {
    static var previews: some View {
        PhotoCell(ListPhoto: DataPhoto[0])
            .previewLayout(.fixed(width: 131, height: 131))
            .padding()
    }
}
