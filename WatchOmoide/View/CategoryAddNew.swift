//
//  CategoryAddNew.swift
//  Omoide
//
//  Created by Mira Novita Dewi Ni Putu on 12/06/21.
//

import SwiftUI

struct CategoryAddNew: View {
    
    @State var text = ""
    var columns = Array(repeating: GridItem(.flexible()), count: 3)
    
    @Environment(\.managedObjectContext) var moc
    @Environment(\.presentationMode) var presen
    
    @State var title = ""
    @State var desc = ""
    @State var photos : Data = .init(count: 0)
    @State var show = false
    @State private var category: String = ""
    
    let model = OmoideImageClassifier()
    
    private func performImageClassification() {
        
        let currentImageName = self.photos
        
        guard let img = UIImage(data: currentImageName) else{
            return
        }
        
        let resizedImage = img.resizeTo(size: CGSize(width: 299, height: 299))
        guard let buffer = resizedImage.toBuffer() else{
            return
        }
        
//        let output = try? model.prediction(image: buffer)
        let output = try? model.prediction(image: buffer)
        
        if let output = output {
            
            self.category = output.classLabel
            // print(self.category)
            
        }
        
    }
    
    var body: some View {
        NavigationView{
            VStack{
                TextField("Category title ...", text: self.$title)
                    .font(.title2)
                    .padding(5)
                
                TextField("Desc ...", text: self.$desc)
                    .font(.caption)
                    .padding(5)
                
                HStack{
                    if photos.count != 0{
                        VStack{
                            HStack{
                                Image(uiImage: UIImage(data: self.photos)!)
                                    .resizable().scaledToFill()
                                    .frame(width: 95, height: 123)
                                    .cornerRadius(14).shadow(radius: 4, x: 0, y: 2)}
                            ZStack {
                                Button(action: {
                                    self.show.toggle()
                                }) {
                                    Text("-")
                                        .font(.title)
                                }
                                .padding(.horizontal, 8)
                                .padding(.top, -3)
                            }.background(Color.white).cornerRadius(50).padding(.top, -80)
                        }
                    } else{
                        VStack{
                            HStack {
                                Button(action: {
                                    self.show.toggle()
                                }) {
                                    Text("+").font(.title)
                                }
                                .foregroundColor(Color("AccentColor")).padding(.horizontal, 8)
                                .padding(.top, -3)
                            }.background(Color.white).cornerRadius(50).padding()
                        }.padding(.vertical, 38).background(LinearGradient(gradient: Gradient(colors: [Color(ColorsSaved.primaryColor), Color(ColorsSaved.secondaryColor)]), startPoint: .top,endPoint: .bottom).cornerRadius(14).frame(width: 95, height: 123).shadow(radius: 4, x: 0, y: 2) .overlay(RoundedRectangle(cornerRadius: 16).stroke(Color.white, lineWidth: 1)))
                    }
                    Spacer()
                }
                .padding(.all, 20)
                
                Button(action: {
                    self.performImageClassification()
                    print(self.category)
                    
                    let send = CategoryData(context: self.moc)
                    send.categoryTitle = self.title
                    send.categoryDesc = self.desc
                    send.categoryPhotos = self.photos
                    send.category = self.category

                    try? self.moc.save()
                    self.presen.wrappedValue.dismiss()
                }){
                    HStack{
                        Text("Save Category")
                            .font(.headline)
                            .foregroundColor(.white)
                            .fontWeight(.bold)
                        
                        Image(systemName: "plus.circle")
                            .foregroundColor(.white)
                    }
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 50)
                    .background(LinearGradient(gradient:Gradient(colors: [Color(ColorsSaved.primaryColor), Color(ColorsSaved.secondaryColor)]),startPoint: .top,endPoint: .bottom))
                    .cornerRadius(15)
                }
                
                Spacer()
                Spacer()
                Spacer()
                Spacer()
                Spacer()
                Spacer()
                
            }
            .padding(15)
            .navigationBarItems(trailing: Button(action:{
                self.presen.wrappedValue.dismiss()
            }){
                Text("Done").bold()
            })
            .sheet(isPresented: self.$show, content: {
                ImagePicker(show: self.$show, photos: self.$photos)
            })
        }
    }
}
