//
//  SearchBar.swift
//  Omoide
//
//  Created by Mira Novita Dewi Ni Putu on 09/06/21.
//

import SwiftUI

struct SearchBar: View {
    @Binding var text: String
    @State private var isEditing = false
    @State var titleOffset: CGFloat = 0
    @State var showSettingView = false
    var body: some View {
        VStack{
            HStack{
                (
                    Text("Memories")
                        .fontWeight(.bold)
                        .foregroundColor(.primary)
                )
                .font(.largeTitle)
                
                Spacer()
                
                Button(action: {
                    self.showSettingView.toggle()
                }) {
                    Image(systemName: "gearshape")
                        .foregroundColor(Color.green)
                }
                
                
            }.padding(.top, 10)
            .padding(.bottom, 5)
            
            HStack{
                TextField("Search here ...", text: $text)
                    .padding(15)
                    .padding(.horizontal, 25)
                    .background(Color(.systemGray6))
                    .foregroundColor(.black)
                    .cornerRadius(8)
                    .overlay(
                        HStack{
                            Image(systemName: "magnifyingglass")
                                .foregroundColor(.gray)
                                .frame(minWidth: 0, maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, alignment: .leading)
                                .padding(.leading, 15)
                            
                            if isEditing{
                                Button(action: {
                                    self.text = ""
                                }, label: {
                                    Image(systemName: "multiply.circle.fill")
                                        .foregroundColor(.gray)
                                        .padding(.trailing, 8)
                                })
                            }
                        }
                    ).onTapGesture {
                        self.isEditing = true
                    }
                
                if isEditing{
                    Button(action:{
                        self.isEditing = false
                        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                    }){
                        Text("Cancel")
                    }
                    .padding(.trailing, 10)
                    .transition(.move(edge: .trailing))
                    .animation(.default)
                }
            }
        }.sheet(isPresented: $showSettingView) {
            SettingView(showSettingView: self.$showSettingView)
        }
    }
}
