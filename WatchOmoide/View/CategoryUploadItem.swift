//
//  CategoryUploadItem.swift
//  Omoide
//
//  Created by Mira Novita Dewi Ni Putu on 10/06/21.
//

import SwiftUI

struct CategoryUploadItem: View {
    
    var body: some View {
        VStack(alignment: .leading, spacing: 7,  content: {
            ZStack{
                VStack{
                    LinearGradient(gradient:
                        Gradient(colors: [Color(ColorsSaved.primaryColor), Color(ColorsSaved.secondaryColor)]),
                                                startPoint: .top,
                                                endPoint: .bottom)
                        .frame(height: 50, alignment: .center)
                        .mask(Image(systemName: "rectangle.stack.badge.plus"))
                        .font(.system(size: 40))
                    
                    Text("Add new category")
                        .foregroundColor(.green)
                        .fontWeight(.medium)
                        .font(.caption)
                }
            }
        })
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity, alignment: .center)
        .padding(.all)
        .overlay(RoundedRectangle(cornerRadius: 12)
        .stroke(LinearGradient(gradient:Gradient(colors: [Color(ColorsSaved.primaryColor), Color(ColorsSaved.secondaryColor)]),startPoint: .top,endPoint: .bottom), lineWidth: 3))
        .padding(.bottom, 13)
    }
}

struct CategoryUploadItem_Previews: PreviewProvider {
    static var previews: some View {
        CategoryUploadItem()
            .previewLayout(.fixed(width: 200, height: 300))
            .padding()
    }
}
