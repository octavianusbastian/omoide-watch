//
//  Memories.swift
//  Omoide
//
//  Created by Mira Novita Dewi Ni Putu on 07/06/21.
//

import SwiftUI

struct Memories: View {
    
    @State var text = ""
    @State var show = false
    
    let items = Array(1...12).map({ "image\($0)" })
    
    let layout = [
        GridItem(.flexible(minimum: 100)),
        GridItem(.flexible(minimum: 100)),
        GridItem(.flexible(minimum: 100))
    ]
    
    var columns = Array(repeating: GridItem(.flexible()), count: 2)
    
    //call model in the view using environtment
    @Environment(\.managedObjectContext) private var moc
    //fetch request to load the data from model
    @FetchRequest(entity: CategoryData.entity(), sortDescriptors: [
        NSSortDescriptor(keyPath: \CategoryData.categoryId, ascending: true),
        NSSortDescriptor(keyPath: \CategoryData.categoryTitle, ascending: true),
        NSSortDescriptor(keyPath: \CategoryData.categoryDesc, ascending: true),
        NSSortDescriptor(keyPath: \CategoryData.categoryPhotos, ascending: true)
    ]) var dataCategory : FetchedResults<CategoryData>
    
    //call image in Data
    @State var image : Data = .init(count: 0)
    
    var body: some View {
        
        NavigationView {
            VStack{
                SearchBar(text: $text).padding(.horizontal, 15)
                
                ScrollView(.vertical, showsIndicators: false){
                    
                    HStack{
                        Text("Category")
                            .font(.body)
                            .fontWeight(.bold)
                        Spacer()
                    }
                    .padding(.horizontal, 15)
                    .padding(.top, 10)
                    
                    LazyVGrid(columns: columns){
                        Button(action: {
                            self.show.toggle()
                        }) {
                            CategoryUploadItem().padding((EdgeInsets(top: 3, leading: 10, bottom: 0, trailing: 0)))
                                .navigationBarBackButtonHidden(true)
                                .navigationBarHidden(true)
                        }
                        ForEach(dataCategory.filter({"\($0)".lowercased().contains(text.lowercased()) || text.isEmpty}), id: \.self){ cat in
                            VStack(alignment: .leading, spacing: 6,  content: {
                                ZStack{
                                    Image(uiImage: UIImage(data: cat.categoryPhotos ?? self.image)!)
                                        .resizable()
                                        .frame(width: .infinity, height: 170)
                                        .scaledToFit()
                                        .cornerRadius(12)
                                    VStack{
                                        Spacer()
                                        Text("10")
                                            .font(.title3)
                                            .foregroundColor(Color.black)
                                            .fontWeight(.bold)
                                            .frame(width: 30, height: 30)
                                            .background(Color.white.opacity(0.4))
                                            .cornerRadius(6)
                                            .padding(.trailing, 7)
                                            .padding(.bottom, 7)
                                    }.frame(minWidth: 0, maxWidth: .infinity, alignment: .trailing)
                                }
                                Text("\(cat.categoryTitle ?? "")")
                                    .font(.title3)
                                    .fontWeight(.black)
                                
                                Text("\(cat.categoryDesc ?? "")")
                                    .font(.caption)
                            })
                            .padding(.bottom, 13)
                        }.padding(EdgeInsets(top:3, leading: 10, bottom: 0, trailing: 0))
                    }
                    .padding(.trailing, 15)
                    .padding(.leading, 6)
                }
            }}
            .sheet(isPresented: self.$show){
                CategoryAddNew().environment(\.managedObjectContext, self.moc)
            }
    }
}

struct Memories_Previews: PreviewProvider {
    static var previews: some View {
        Memories()
    }
}

struct mCategories: Identifiable{
    var id = UUID()
    var categoryTitle: String
    var categoryDesc: String
    var categoryPhotos: String
}

struct Category: Identifiable{
    var id = UUID()
    var title: String
    var image: String
    var desc: String
}

var mCategoryData = [
    Category(title: "Special Person", image: "image-1", desc: "A collection of beautiful memories that I have"),
    Category(title: "Special Moment", image: "image-2", desc: "A collection of beautiful memories that I have"),
    Category(title: "Family", image: "image-3", desc: "A collection of beautiful memories that I have"),
    Category(title: "Vacation", image: "image-4", desc: "A collection of beautiful memories that I have"),
    Category(title: "Boyfriend", image: "", desc: "A collection of beautiful memories that I have"),
    Category(title: "Boyfriend", image: "", desc: "A collection of beautiful memories that I have"),
    Category(title: "Boyfriend", image: "", desc: "A collection of beautiful memories that I have"),
    Category(title: "Boyfriend", image: "", desc: "A collection of beautiful memories that I have")
]


