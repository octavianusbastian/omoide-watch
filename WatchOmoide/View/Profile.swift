//
//  Profile.swift
//  Omoide
//
//  Created by Natanael Kenan Gunawan on 08/06/21.
//

import SwiftUI

//    @State private var editprofile = ""
//    @State private var notification = ""
//    @State private var language = ""

struct Profile: View {
    
    @State private var editprofile = ""
    @State private var notification = ""
    @State private var language = ""
    
    
    var body: some View {
        NavigationView{
            VStack{
                ProfileImage(imageName: "fp")
                    .padding()
                
                Form{
                    
                    Section(header: Text("Edit")){
                        TextField("Edit Profile", text: $editprofile)
                        TextField("Notification", text: $notification)
                        TextField("Language", text: $language)
                        }
                    //                    Spacer().frame(height: 30)
                    
                    //                    Button {
                    //                        print("Button Tapped")
                    //                    } label: {
                    //                        Text("Log Out")
                    //                            .bold()
                    //                            .frame(width:260, height: 50)
                    //                            .background(Color.black)
                    //                            .foregroundColor(.white)
                    //                            .cornerRadius(12)
                    //                            .padding(.horizontal,60)
                    
                }

                Button {
                    print("Button Tapped")
                } label: {
                    Text("Log Out")
                        .bold()
                        .frame(width:260, height: 50)
                        .background(Color.black)
                        .foregroundColor(.white)
                        .cornerRadius(12)
                        .padding(.horizontal,60)
                    
                }.offset(y:-200)
            }
            .navigationTitle("Profile")
        }
    }
    
    struct Profile_Previews: PreviewProvider {
        static var previews: some View {
            Profile()
        }
    }
    struct ProfileImage: View{
        var imageName: String
        
        var body: some View {
            Image(imageName)
                .resizable()
                .frame(width: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/)
                .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
        }
    }
}
