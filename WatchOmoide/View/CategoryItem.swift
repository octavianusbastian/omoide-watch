//
//  CategoryItem.swift
//  Omoide
//
//  Created by Mira Novita Dewi Ni Putu on 09/06/21.
//

//import SwiftUI
//
//struct CategoryItem: View {
//    
//    @Binding var categoryPhotos: String
//    @Binding var categoryTitle: String
//    @Binding var categoryDesc: String
//
//
//    var body: some View {
//        
//        VStack(alignment: .leading, spacing: 6,  content: {
//            ZStack{
//                Image(uiImage: UIImage(photos: $categoryPhotos)!)
//                    .resizable()
//                    .scaledToFit()
//                    .cornerRadius(12)
//                VStack{
//                    Spacer()
//                    Text("10")
//                        .font(.title3)
//                        .foregroundColor(Color.black)
//                        .fontWeight(.bold)
//                        .frame(width: 30, height: 30)
//                        .background(Color.white.opacity(0.4))
//                        .cornerRadius(6)
//                        .padding(.trailing, 7)
//                        .padding(.bottom, 7)
//                }.frame(minWidth: 0, maxWidth: .infinity, alignment: .trailing)
//            }
//            Text(title: $categoryTitle)
//                .font(.title3)
//                .fontWeight(.black)
//            
//            Text(desc: $categoryDesc)
//                .font(.caption)
//        })
//        .padding(.bottom, 13)
//    }
//}


//
//  CategoryItem.swift
//  Omoide
//
//  Created by Mira Novita Dewi Ni Putu on 09/06/21.
//

//import SwiftUI
//
//struct CategoryItem: View {
//
//    let category: Category
//    let categories: mCategories
//
//    var body: some View {
//
//        VStack(alignment: .leading, spacing: 6,  content: {
//            ZStack{
//                Image(categories.categoryPhotos)
//                    .resizable()
//                    .scaledToFit()
//                    .cornerRadius(12)
//                VStack{
//                    Spacer()
//                    Text("10")
//                        .font(.title3)
//                        .foregroundColor(Color.black)
//                        .fontWeight(.bold)
//                        .frame(width: 30, height: 30)
//                        .background(Color.white.opacity(0.4))
//                        .cornerRadius(6)
//                        .padding(.trailing, 7)
//                        .padding(.bottom, 7)
//                }.frame(minWidth: 0, maxWidth: .infinity, alignment: .trailing)
//            }
//            Text(categories.categoryTitle)
//                .font(.title3)
//                .fontWeight(.black)
//
//            Text(categories.)
//                .font(.caption)
//        })
//        .padding(.bottom, 13)
//    }
//}
//
//struct CategoryItem_Previews: PreviewProvider {
//    static var previews: some View {
//        CategoryItem(category: mCategoryData[0])
//            .previewLayout(.fixed(width: 200, height: 300))
//            .padding()
//    }
//}
