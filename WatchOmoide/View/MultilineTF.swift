//
//  MultilineTextView.swift
//  Omoide
//
//  Created by Mira Novita Dewi Ni Putu on 12/06/21.
//

import SwiftUI

struct MultilineTF: UIViewRepresentable {
    
    @Binding var txt: String
    
    func makeCoordinator() -> MultilineTF.Coordinator {
        return MultilineTF.Coordinator(parent1: self)
    }
    
    func makeUIView(context: UIViewRepresentableContext<MultilineTF>) -> UITextView {
        let tview = UITextView()
        tview.isEditable = true
        tview.isUserInteractionEnabled = true
        tview.isScrollEnabled = true
        tview.text = "Write description for category"
        tview.textColor = .gray
        tview.font = .systemFont(ofSize: 15)
        return tview
    }
    
    func updateUIView(_ uiView: UITextView, context: UIViewRepresentableContext<MultilineTF>) {
        
    }
    
    class Coordinator: NSObject, UITextViewDelegate {
        var parent: MultilineTF
        
        init(parent1: MultilineTF){
            parent = parent1
        }
        
        func textViewDidChange(_ textView: UITextView) {
            self.parent.txt = textView.text
        }
        
        func textViewDidBeginEditing(_ textView: UITextView) {
            textView.text = ""
            textView.textColor = .label
        }
    }
}
