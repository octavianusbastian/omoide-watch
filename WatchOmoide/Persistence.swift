//
//  Persistence.swift
//  Omoide
//
//  Created by Yanuar Tanzil on 14/06/21.
//

import Foundation
//
//  Persistence.swift
//  Coredataf
//
//  Created by Yanuar Tanzil on 14/06/21.
//

import CoreData

//struct PersistenceController {
//    static let shared = PersistenceController()
//
//    let container: NSPersistentCloudKitContainer
//
//    init(inMemory: Bool = false) {
//        container = NSPersistentCloudKitContainer(name: "Category")
//        if inMemory {
//            container.persistentStoreDescriptions.first!.url = URL(fileURLWithPath: "/dev/null")
//        }
//        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
//            if let error = error as NSError? {
//                fatalError("Unresolved error \(error), \(error.userInfo)")
//            }
//        })
//
//        container.viewContext.automaticallyMergesChangesFromParent = true
//        container.viewContext.mergePolicy = NSMergeByPropertyOb
//    }
//}
//

struct PersistenceController {
    static let shared = PersistenceController()

    let container: NSPersistentCloudKitContainer

    init(inMemory: Bool = false) {
        container = NSPersistentCloudKitContainer(name: "Category")
        if inMemory {
            // Create a store description for a local store
            let localStoreLocation = URL(fileURLWithPath: "/dev/null")
            let localStoreDescription = NSPersistentStoreDescription(url: localStoreLocation)
            localStoreDescription.configuration = "Local"
            // Create a store description for a CloudKit-backed local store
            let cloudStoreLocation = URL(fileURLWithPath: "/dev/null")
            let cloudStoreDescription =
                NSPersistentStoreDescription(url: cloudStoreLocation)
            cloudStoreDescription.configuration = "Cloud"
            // Set the container options on the cloud store
            cloudStoreDescription.cloudKitContainerOptions = NSPersistentCloudKitContainerOptions(containerIdentifier: "iCloud.com.miranovitad.watchOmoide")
            // Update the container's list of store descriptions
            container.persistentStoreDescriptions = [
                cloudStoreDescription,
                localStoreDescription
            ]
            
        }
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            guard error == nil else {
                fatalError("Could not load persistent stores. \(error!)")
            }
        })
        container.viewContext.automaticallyMergesChangesFromParent = true
        container.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
    }
}
