//
//  WatchCategoryItem.swift
//  WatchOmoide WatchKit Extension
//
//  Created by Octavianus Bastian on 14/06/21.
//

import SwiftUI

struct WatchCategoryItem: View {
    let category: Category
    
    var body: some View {
        
        VStack(alignment: .leading, spacing: 6,  content: {
            Text(category.title)
                .font(.footnote).frame(width: 163, height: 36, alignment: .leading).padding(.leading, 15).padding(.vertical, 3).background(Color(ColorsSaved.grayColor)).cornerRadius(6)
        })
        .padding(.bottom, 8)
    }
}

struct WatchCategoryItem_Previews: PreviewProvider {
    static var previews: some View {
        WatchCategoryItem(category: WatchCategoryData[0])
    }
}





