//
//  WatchMemories.swift
//  WatchOmoide WatchKit Extension
//
//  Created by Octavianus Bastian on 14/06/21.
//

import SwiftUI
import UserNotifications

struct WatchMemories: View {
    
    let layout = [
        GridItem(.flexible(minimum: 100)),
        GridItem(.flexible(minimum: 100)),
        GridItem(.flexible(minimum: 100))
    ]
    var columns = Array(repeating: GridItem(.flexible()), count: 1)
    
    //call model in the view using environtment
    @Environment(\.managedObjectContext) private var moc
    //fetch request to load the data from model
    @FetchRequest(entity: CategoryData.entity(), sortDescriptors: [
        NSSortDescriptor(keyPath: \CategoryData.categoryId, ascending: true),
        NSSortDescriptor(keyPath: \CategoryData.categoryTitle, ascending: true),
        NSSortDescriptor(keyPath: \CategoryData.categoryDesc, ascending: true),
        NSSortDescriptor(keyPath: \CategoryData.categoryPhotos, ascending: true),
        NSSortDescriptor(keyPath: \CategoryData.category, ascending: true)
    ]) var dataCategory : FetchedResults<CategoryData>
    
    //call image in Data
    @State var image : Data = .init(count: 0)
    
//    struct Quote: Codable {
//        var quote: String
//    }
//    
    var body: some View {
        
        ScrollView(.vertical, showsIndicators: false) {
            
            VStack {
                HStack {
                    VStack {
                        //                        MyListItem(cat: .constant(dataCategory.randomElement()!))
                        //                        ForEach(dataCategory, id: \.self){ cat in
                        //                            VStack(alignment: .leading, spacing: 6,  content: {
                        //                                ZStack{
                        //                                    Image(uiImage: UIImage(data: cat.categoryPhotos ?? self.image)!)
                        //                                        .resizable()
                        //                                        .frame(width: .infinity, height: 170)
                        //                                        .scaledToFit()
                        //                                        .cornerRadius(12)
                        //                                    VStack{
                        //                                        Spacer()
                        //                                        Text("10")
                        //                                            .font(.title3)
                        //                                            .foregroundColor(Color.black)
                        //                                            .fontWeight(.bold)
                        //                                            .frame(width: 30, height: 30)
                        //                                            .background(Color.white.opacity(0.4))
                        //                                            .cornerRadius(6)
                        //                                            .padding(.trailing, 7)
                        //                                            .padding(.bottom, 7)
                        //                                    }.frame(minWidth: 0, maxWidth: .infinity, alignment: .trailing)
                        //                                }
                        //                                Text("\(cat.categoryTitle ?? "")")
                        //                                    .font(.title3)
                        //                                    .fontWeight(.black)
                        //
                        //                                Text("\(cat.category ?? "")")
                        //                                    .font(.title3)
                        //                                    .fontWeight(.black)
                        //
                        //                                Text("\(cat.categoryDesc ?? "")")
                        //                                    .font(.caption)
                        //                            })
                        //                            .padding(.bottom, 13)
                        //                        }.padding(EdgeInsets(top:3, leading: 10, bottom: 0, trailing: 0))
                        
                        Button("Boost Me"){
                            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]){
                                success, error in
                                if success{
                                    print("All set!")
//                                    let quotes = Bundle.main.decode([Quote].self, from: "quotes.json")
//                                    var quote : Quote { quotes.randomElement()! }
                                    let content = UNMutableNotificationContent()
                                    
                                    content.title = "Knock, Knock"
                                    content.body = "Test message"
                                    content.sound = UNNotificationSound.default
                                    content.categoryIdentifier = "myCategory"
                                    content.threadIdentifier = "5280"
                                    
                                    
                                    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
                                    
                                    let request = UNNotificationRequest(identifier: "UUID().uuidString", content: content, trigger: trigger)
                                    
                                    UNUserNotificationCenter.current().add(request)
                                } else if let error = error{
                                    print(error.localizedDescription)
                                }
                            }
                        }.buttonStyle(PlainButtonStyle())}.foregroundColor(.black).frame(width: 100, height: 100, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).background(Color(ColorsSaved.primaryColor).cornerRadius(100 / 2 ))
                }.padding(.top, 5)
                HStack(alignment: .center) {
                    Spacer()
                    NavigationLink(
                        destination: JournalView()) {
                        VStack(alignment: .center) {
                            Image(systemName: "photo").foregroundColor(.black).frame(width: 60, height: 60, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).background(Color(ColorsSaved.primaryColor)).cornerRadius(30); Text("Album").padding(.top, 1)
                                .font(.caption2)}.foregroundColor(Color(ColorsSaved.primaryColor))}.buttonStyle(PlainButtonStyle()).padding(.top, 5).padding(.leading, 10)
                    Spacer()
                    Spacer()
                    NavigationLink(
                        destination: WatchSettingView()) {
                        VStack(alignment: .center) {
                            Image(systemName: "gearshape").foregroundColor(.black).frame(width: 60, height: 60, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).background(Color(ColorsSaved.primaryColor)).cornerRadius(30); Text("Setting").padding(.top, 1)
                                .font(.caption2)}.foregroundColor(Color(ColorsSaved.primaryColor))
                        Spacer()
                    }.padding(.top, 5).padding(.leading, 20).buttonStyle(PlainButtonStyle()) }
                
            }.onAppear(perform: {
//                let quotes = Bundle.main.decode([Quote].self, from: "quotes.json")
//                var quote : Quote { quotes.randomElement()! }
//                scheduleNotifications(quote: quote.quote)
                                scheduleNotifications()
            }).navigationBarTitle(Text("Omoide"))}
    }
}

struct WatchMemories_Previews: PreviewProvider {
    static var previews: some View {
        WatchMemories()
    }
}

struct  MyListItem:View{
    @Binding var cat:CategoryData
    @State var image : Data = .init(count: 0)
    var body: some View{
        VStack(alignment: .leading, spacing: 6,  content: {
            ZStack{
                Image(uiImage: UIImage(data: cat.categoryPhotos ?? self.image)!)
                    .resizable()
                    .frame(width: .infinity, height: 170)
                    .scaledToFit()
                    .cornerRadius(12)
                VStack{
                    Spacer()
                    Text("10")
                        .font(.title3)
                        .foregroundColor(Color.black)
                        .fontWeight(.bold)
                        .frame(width: 30, height: 30)
                        .background(Color.white.opacity(0.4))
                        .cornerRadius(6)
                        .padding(.trailing, 7)
                        .padding(.bottom, 7)
                }.frame(minWidth: 0, maxWidth: .infinity, alignment: .trailing)
            }
            Text("\(cat.categoryTitle ?? "")")
                .font(.title3)
                .fontWeight(.black)
            
            Text("\(cat.category ?? "")")
                .font(.title3)
                .fontWeight(.black)
            
            Text("\(cat.categoryDesc ?? "")")
                .font(.caption)
        })
        .padding(.bottom, 13)
        //                        }.padding(EdgeInsets(top:3, leading: 10, bottom: 0, trailing: 0))
    }
}


func scheduleNotifications() {
    UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { success, error in
        if success {
            print("Success")
            //To add badgeNumber
            //UIApplication.shared.applicationIconBadgeNumber = badgeNumber (Integer Value)
            UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
            let content = UNMutableNotificationContent()
            content.title = "Knock, Knock 😇"
            content.body = "Daily Notification is Ready"
            content.sound = UNNotificationSound.default
            content.categoryIdentifier = "myCategory"
            content.threadIdentifier = "5280"
            
            var dateComponents = DateComponents()
            dateComponents.hour = 23
            dateComponents.minute = 33
            let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
            //            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
            let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
            UNUserNotificationCenter.current().add(request)
        } else if let error = error {
            print(error.localizedDescription)
        }
    }
    
    
    
}

//extension Bundle {
//    func decode<T: Decodable>(_ type: T.Type, from file: String, dateDecodingStrategy: JSONDecoder.DateDecodingStrategy = .deferredToDate, keyDecodingStrategy: JSONDecoder.KeyDecodingStrategy = .useDefaultKeys) -> T {
//        guard let url = self.url(forResource: file, withExtension: nil) else {
//            fatalError("Failed to locate \(file) in bundle.")
//        }
//
//        guard let data = try? Data(contentsOf: url) else {
//            fatalError("Failed to load \(file) from bundle.")
//        }
//
//        let decoder = JSONDecoder()
//        decoder.dateDecodingStrategy = dateDecodingStrategy
//        decoder.keyDecodingStrategy = keyDecodingStrategy
//
//        do {
//            return try decoder.decode(T.self, from: data)
//        } catch DecodingError.keyNotFound(let key, let context) {
//            fatalError("Failed to decode \(file) from bundle due to missing key '\(key.stringValue)' not found – \(context.debugDescription)")
//        } catch DecodingError.typeMismatch(_, let context) {
//            fatalError("Failed to decode \(file) from bundle due to type mismatch – \(context.debugDescription)")
//        } catch DecodingError.valueNotFound(let type, let context) {
//            fatalError("Failed to decode \(file) from bundle due to missing \(type) value – \(context.debugDescription)")
//        } catch DecodingError.dataCorrupted(_) {
//            fatalError("Failed to decode \(file) from bundle because it appears to be invalid JSON")
//        } catch {
//            fatalError("Failed to decode \(file) from bundle: \(error.localizedDescription)")
//        }
//    }
//}
