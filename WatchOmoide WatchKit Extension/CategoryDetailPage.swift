//
//  CategoryDetailPage.swift
//  WatchOmoide WatchKit Extension
//
//  Created by Octavianus Bastian on 14/06/21.
//

import SwiftUI

struct CategoryDetailPage: View {
    
    @State var text = ""
    let items = Array(1...12).map({ "image\($0)" })
    
    let layout = [
        GridItem(.flexible(minimum: 100)),
        GridItem(.flexible(minimum: 100)),
        GridItem(.flexible(minimum: 100))
    ]
    var columns = Array(repeating: GridItem(.flexible()), count: 2)
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack {
                VStack {
                    LazyVGrid(columns: columns){
                        ForEach(WatchCategoryData.filter({"\($0)".lowercased().contains(text.lowercased()) || text.isEmpty})){ i in
                            CategoryDetailView(category: i)
                        }
                    }
                }.padding(.top, 10)
            }.navigationBarTitle(Text("Uhuy"))}
    }
    
}

struct CategoryDetailPage_Previews: PreviewProvider {
    static var previews: some View {
        CategoryDetailPage()
    }
}

