//
//  WatchOmoideApp.swift
//  WatchOmoide WatchKit Extension
//
//  Created by Octavianus Bastian on 14/06/21.
//

import SwiftUI

@main
struct WatchOmoideApp: App {
    
    let persistenceController = PersistenceController.shared
    
    @SceneBuilder var body: some Scene {
        WindowGroup {
            NavigationView {
                WatchMemories().environment(\.managedObjectContext, persistenceController.container.viewContext)
            }
        }

        WKNotificationScene(controller: NotificationController.self, category: "myCategory")
    }
}
