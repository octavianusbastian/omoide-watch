//
//  NotificationView.swift
//  WatchOmoide WatchKit Extension
//
//  Created by Octavianus Bastian on 14/06/21.
//

import SwiftUI

struct NotificationView: View {
    
    let layout = [
        GridItem(.flexible(minimum: 100)),
        GridItem(.flexible(minimum: 100)),
        GridItem(.flexible(minimum: 100))
    ]
    var columns = Array(repeating: GridItem(.flexible()), count: 1)
    
    //call model in the view using environtment
    //    @Environment(\.managedObjectContext) private var moc
    //    //fetch request to load the data from model
    //    @FetchRequest(entity: CategoryData.entity(), sortDescriptors: [
    //        NSSortDescriptor(keyPath: \CategoryData.categoryId, ascending: true),
    //        NSSortDescriptor(keyPath: \CategoryData.categoryTitle, ascending: true),
    //        NSSortDescriptor(keyPath: \CategoryData.categoryDesc, ascending: true),
    //        NSSortDescriptor(keyPath: \CategoryData.categoryPhotos, ascending: true),
    //        NSSortDescriptor(keyPath: \CategoryData.category, ascending: true)
    //    ]) var dataCategory : FetchedResults<CategoryData>
    
    @Binding var dataCategory:NoticationViewModel?
    
    //    var imageData: [String]
    
    //call image in Data
    @State var image : Data = .init(count: 0)
    
    struct Quote: Codable {
        var quote: String
    }
    
    let quotes = Bundle.main.decode([Quote].self, from: "quotes.json")
    var quote : Quote { quotes.randomElement()! }
    
    var body: some View {
        
        ScrollView(.vertical, showsIndicators: false) {
            VStack(alignment: .center, content: {
                //                Text(dataCategory!.data!.categoryTitle ?? "")
                
                //                Image(uiImage: UIImage(data: ?? self.image)!)
                //                    .resizable()
                //                    .scaledToFit()
                //                    .frame(width: 170, height: 170)
                //                    .cornerRadius(15)
                
                Image("image-3")
                    .resizable().scaledToFill()
                    .frame(width: 170, height: 170)
                    .cornerRadius(15)
                
            }
            )
            
            VStack(alignment: .leading, content: {
                Text(quote.quote)
                    .font(.body)
                    .fontWeight(.semibold)
//                Text("Ada yang mau bertemu nich 😊")
//                    .font(.footnote)
//
                HStack {
                    VStack(alignment: .center) {
                        Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                            Text("😊")
                        }).foregroundColor(.black).frame(width: 40, height: 40, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).background(Color(ColorsSaved.primaryColor)).cornerRadius(15)}.foregroundColor(Color(ColorsSaved.primaryColor))
                    VStack(alignment: .center) {
                        Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                            Text("😭")
                        }).foregroundColor(.black).frame(width: 40, height: 40, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).background(Color(ColorsSaved.primaryColor)).cornerRadius(15)}.foregroundColor(Color(ColorsSaved.primaryColor))
                    VStack(alignment: .center) {
                        Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                            Text("😇")
                        }).foregroundColor(.black).frame(width: 40, height: 40, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).background(Color(ColorsSaved.primaryColor)).cornerRadius(15)}.foregroundColor(Color(ColorsSaved.primaryColor))
                    VStack(alignment: .center) {
                        Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                            Text("😘")
                        }).foregroundColor(.black).frame(width: 40, height: 40, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).background(Color(ColorsSaved.primaryColor)).cornerRadius(15)}.foregroundColor(Color(ColorsSaved.primaryColor))
                }.padding(.top, 5)
                HStack {
                    VStack(alignment: .center) {
                        Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                            Text("😜")
                        }).foregroundColor(.black).frame(width: 40, height: 40, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).background(Color(ColorsSaved.primaryColor)).cornerRadius(15)}.foregroundColor(Color(ColorsSaved.primaryColor))
                    VStack(alignment: .center) {
                        Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                            Text("😤")
                        }).foregroundColor(.black).frame(width: 40, height: 40, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).background(Color(ColorsSaved.primaryColor)).cornerRadius(15)}.foregroundColor(Color(ColorsSaved.primaryColor))
                    VStack(alignment: .center) {
                        Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                            Text("😱")
                        }).foregroundColor(.black).frame(width: 40, height: 40, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).background(Color(ColorsSaved.primaryColor)).cornerRadius(15)}.foregroundColor(Color(ColorsSaved.primaryColor))
                    VStack(alignment: .center) {
                        Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                            Text("😎")
                        }).foregroundColor(.black).frame(width: 40, height: 40, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).background(Color(ColorsSaved.primaryColor)).cornerRadius(15)}.foregroundColor(Color(ColorsSaved.primaryColor))
                }.padding(.top, 5)
            }
            
            )
        }
    }
}

//struct NotificationView_Previews: PreviewProvider {
//    static var previews: some View {
//        NotificationView()
//    }
//}

extension Bundle {
    func decode<T: Decodable>(_ type: T.Type, from file: String, dateDecodingStrategy: JSONDecoder.DateDecodingStrategy = .deferredToDate, keyDecodingStrategy: JSONDecoder.KeyDecodingStrategy = .useDefaultKeys) -> T {
        guard let url = self.url(forResource: file, withExtension: nil) else {
            fatalError("Failed to locate \(file) in bundle.")
        }
        
        guard let data = try? Data(contentsOf: url) else {
            fatalError("Failed to load \(file) from bundle.")
        }
        
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = dateDecodingStrategy
        decoder.keyDecodingStrategy = keyDecodingStrategy
        
        do {
            return try decoder.decode(T.self, from: data)
        } catch DecodingError.keyNotFound(let key, let context) {
            fatalError("Failed to decode \(file) from bundle due to missing key '\(key.stringValue)' not found – \(context.debugDescription)")
        } catch DecodingError.typeMismatch(_, let context) {
            fatalError("Failed to decode \(file) from bundle due to type mismatch – \(context.debugDescription)")
        } catch DecodingError.valueNotFound(let type, let context) {
            fatalError("Failed to decode \(file) from bundle due to missing \(type) value – \(context.debugDescription)")
        } catch DecodingError.dataCorrupted(_) {
            fatalError("Failed to decode \(file) from bundle because it appears to be invalid JSON")
        } catch {
            fatalError("Failed to decode \(file) from bundle: \(error.localizedDescription)")
        }
    }
}
