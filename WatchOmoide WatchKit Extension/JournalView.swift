//
//  JournalView.swift
//  WatchOmoide WatchKit Extension
//
//  Created by Octavianus Bastian on 14/06/21.
//

import SwiftUI

struct JournalView: View {
    @State var text = ""
    let items = Array(1...12).map({ "image\($0)" })
    
    let layout = [
        GridItem(.flexible(minimum: 100)),
        GridItem(.flexible(minimum: 100)),
        GridItem(.flexible(minimum: 100))
    ]
    var columns = Array(repeating: GridItem(.flexible()), count: 1)
    var body: some View {
        
        ScrollView(.vertical, showsIndicators: false) {
            VStack {
                VStack {
                    LazyVGrid(columns: columns){
                        ForEach(WatchCategoryData.filter({"\($0)".lowercased().contains(text.lowercased()) || text.isEmpty})){ i in
                            WatchCategoryItem(category: i)
                        }
                    }
                }.padding(.top, 10)
            }.navigationBarTitle(Text("Album"))}
    }
}

struct JournalView_Previews: PreviewProvider {
    static var previews: some View {
        JournalView()
    }
}

struct Category: Identifiable{
    var id = UUID()
    var title: String
    var image: String
    var desc: String
}

var WatchCategoryData = [
    Category(title: "Bali Yeay!!", image: "", desc: "A collection of beautiful memories that I have"),
    Category(title: "Special Person", image: "image-1", desc: "A collection of beautiful memories that I have"),
    Category(title: "Special Moment", image: "image-2", desc: "A collection of beautiful memories that I have"),
]
