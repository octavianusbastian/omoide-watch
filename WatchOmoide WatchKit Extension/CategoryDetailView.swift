//
//  CategoryDetailView.swift
//  WatchOmoide WatchKit Extension
//
//  Created by Octavianus Bastian on 14/06/21.
//

import SwiftUI

struct CategoryDetailView: View {
    let category: Category
    var body: some View {
        VStack {
            Image(category.image)
                .resizable().scaledToFill()
                .frame(width: 80, height: 80)
                .cornerRadius(15)}
    }
}



struct CategoryDetailView_Previews: PreviewProvider {
    static var previews: some View {
        CategoryDetailView(category: WatchCategoryData[0])
    }
}
