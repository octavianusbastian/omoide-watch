//
//  KnockKnockView.swift
//  WatchOmoide WatchKit Extension
//
//  Created by Octavianus Bastian on 16/06/21.
//

import SwiftUI

struct KnockKnockView: View {
    var body: some View {
            ScrollView(.vertical, showsIndicators: false) {
                VStack(alignment: .center, content: {
                    
                    Image("image-1")
                        .resizable().scaledToFill()
                        .frame(width: 170, height: 170)
                        .cornerRadius(15)
                    
                }
                )
                
                VStack(alignment: .leading, content: {
                    
                    
                    Text("Knock knock,")
                        .font(.body)
                        .fontWeight(.semibold)
                    Text("Ada yang mau bertemu nich 😊")
                        .font(.footnote)
                    
                    HStack {
                        VStack(alignment: .center) {
                            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                                Text("😊")
                            }).foregroundColor(.black).frame(width: 40, height: 40, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).background(Color(ColorsSaved.primaryColor)).cornerRadius(15)}.foregroundColor(Color(ColorsSaved.primaryColor))
                        VStack(alignment: .center) {
                            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                                Text("😭")
                            }).foregroundColor(.black).frame(width: 40, height: 40, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).background(Color(ColorsSaved.primaryColor)).cornerRadius(15)}.foregroundColor(Color(ColorsSaved.primaryColor))
                        VStack(alignment: .center) {
                            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                                Text("😇")
                            }).foregroundColor(.black).frame(width: 40, height: 40, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).background(Color(ColorsSaved.primaryColor)).cornerRadius(15)}.foregroundColor(Color(ColorsSaved.primaryColor))
                        VStack(alignment: .center) {
                            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                                Text("😘")
                            }).foregroundColor(.black).frame(width: 40, height: 40, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).background(Color(ColorsSaved.primaryColor)).cornerRadius(15)}.foregroundColor(Color(ColorsSaved.primaryColor))
                    }.padding(.top, 5)
                    HStack {
                        VStack(alignment: .center) {
                            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                                Text("😜")
                            }).foregroundColor(.black).frame(width: 40, height: 40, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).background(Color(ColorsSaved.primaryColor)).cornerRadius(15)}.foregroundColor(Color(ColorsSaved.primaryColor))
                        VStack(alignment: .center) {
                            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                                Text("😤")
                            }).foregroundColor(.black).frame(width: 40, height: 40, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).background(Color(ColorsSaved.primaryColor)).cornerRadius(15)}.foregroundColor(Color(ColorsSaved.primaryColor))
                        VStack(alignment: .center) {
                            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                                Text("😱")
                            }).foregroundColor(.black).frame(width: 40, height: 40, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).background(Color(ColorsSaved.primaryColor)).cornerRadius(15)}.foregroundColor(Color(ColorsSaved.primaryColor))
                        VStack(alignment: .center) {
                            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                                Text("😎")
                            }).foregroundColor(.black).frame(width: 40, height: 40, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).background(Color(ColorsSaved.primaryColor)).cornerRadius(15)}.foregroundColor(Color(ColorsSaved.primaryColor))
                    }.padding(.top, 5)
                }
                
                )
            }.navigationTitle("SOS")
    }
}

struct KnockKnockView_Previews: PreviewProvider {
    static var previews: some View {
        KnockKnockView()
    }
}
