//
//  WatchSettingView.swift
//  WatchOmoide WatchKit Extension
//
//  Created by Octavianus Bastian on 15/06/21.
//

import SwiftUI
import UserNotifications

struct WatchSettingView: View {
    @State private var previewIndex = 0
    var previewOptions = ["Always", "When Unlocked", "Never"]
    var previewbhs = ["English", "Japanese", "Bahasa Indonesia"]
    var haloo = ["Knock Knock", "Konnichiwa", "Ding Dong"]
    
    
    
    
    var body: some View {
        VStack {
            Form{
                Section(header: Text("Edit")){
                    Picker(selection: $previewIndex, label: Text("Notification")) {
                        ForEach(0 ..< previewOptions.count) {
                            Text(self.previewOptions[$0])
                            
                            
                        }
                    }
                    Picker(selection: $previewIndex, label: Text("Language")) {
                        ForEach(0 ..< previewbhs.count) {
                            Text(self.previewbhs[$0])
                            
                            
                        }
                    }
//                    Button(action: scheduleNotifications, label: {
//                        Text("Set Reminder")
//                    })
                    Picker(selection: $previewIndex, label: Text("Sentences")) {
                        ForEach(0 ..< haloo.count) {
                            Text(self.haloo[$0])


                        }
                    }
                }
                Section(header: Text("ABOUT")) {
                    HStack {
                        Text("Version")
                        Spacer()
                        Text("0.0.1")
                    }
                }
            }.padding(.top, 5)
        }.navigationTitle("Setting")
    }
}
struct WatchSettingView_Previews: PreviewProvider {
    static var previews: some View {
        WatchSettingView()
    }
}


