//
//  CategoryData+CoreDataProperties.swift
//  WatchOmoide
//
//  Created by Mira Novita Dewi Ni Putu on 14/06/21.
//
//

import Foundation
import CoreData


extension CategoryData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CategoryData> {
        return NSFetchRequest<CategoryData>(entityName: "CategoryData")
    }
    
    @NSManaged public var category: String?
    @NSManaged public var categoryDesc: String?
    @NSManaged public var categoryId: UUID?
    @NSManaged public var categoryPhotos: Data?
    @NSManaged public var categoryTitle: String?
    

}

extension CategoryData : Identifiable {

}
